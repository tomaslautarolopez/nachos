/// Routines for synchronizing threads.
///
/// The implementation for this primitive does not come with base Nachos.
/// It is left to the student.
///
/// When implementing this module, keep in mind that any implementation of a
/// synchronization routine needs some primitive atomic operation.  The
/// semaphore implementation, for example, disables interrupts in order to
/// achieve this; another way could be leveraging an already existing
/// primitive.
///
/// Copyright (c) 1992-1993 The Regents of the University of California.
///               2016-2021 Docentes de la Universidad Nacional de Rosario.
/// All rights reserved.  See `copyright.h` for copyright notice and
/// limitation of liability and disclaimer of warranty provisions.

#include "condition.hh"
/// Dummy functions -- so we can compile our later assignments.
///
/// Note -- without a correct implementation of `Condition::Wait`, the test
/// case in the network assignment will not work!

Condition::Condition(const char *debugName, Lock *conditionLock_)
{
    name = debugName;
    conditionLock = conditionLock_;
    waiters = new List<Semaphore *>();
}

Condition::~Condition()
{
    while (!waiters->IsEmpty())
    {
        delete waiters->Pop();
    }
    delete waiters;
}

const char *
Condition::GetName() const
{
    return name;
}

void Condition::Wait()
{
    ASSERT(conditionLock->IsHeldByCurrentThread());

    char *semaphoreName = new char[64];
    snprintf(semaphoreName, 64, "Condition Variable %s Semaphore of Thread %s",
             GetName(), currentThread->GetName());
    Semaphore *sempahore = new Semaphore(semaphoreName, 0);

    waiters->Append(sempahore);
    conditionLock->Release();
    sempahore->P();

    conditionLock->Acquire();
    delete semaphoreName;
    delete sempahore;
}

void Condition::Signal()
{
    ASSERT(conditionLock->IsHeldByCurrentThread());

    if (!waiters->IsEmpty())
    {
        waiters->Pop()->V();
    }
}

void Condition::Broadcast()
{
    ASSERT(conditionLock->IsHeldByCurrentThread());

    while (!waiters->IsEmpty())
    {
        waiters->Pop()->V();
    }
}
