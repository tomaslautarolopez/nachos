/// Copyright (c) 1992-1993 The Regents of the University of California.
///               2007-2009 Universidad de Las Palmas de Gran Canaria.
///               2016-2021 Docentes de la Universidad Nacional de Rosario.
/// All rights reserved.  See `copyright.h` for copyright notice and
/// limitation of liability and disclaimer of warranty provisions.

#include "thread_test_simple.hh"
#include "semaphore.hh"
#include "system.hh"

#include <stdio.h>
#include <string.h>

#define SIMPLE_NUM_THREADS 5

/// Loop 10 times, yielding the CPU to another ready thread each iteration.
///
/// * `name` points to a string with a thread name, just for debugging
///   purposes.
void SimpleThread(void *args)
{
    // Reinterpret arg `name` as a string.
    char *name = (char *)(((void **)args)[0]);
    Semaphore *semaphore = (Semaphore *)(((void **)args)[1]);

    // If the lines dealing with interrupts are commented, the code will
    // behave incorrectly, because printf execution may cause race
    // conditions.

#ifdef SEMAPHORE_TEST
    semaphore->P();
    DEBUG('t', "Thread %s is calling P on sempahore %s\n", name, semaphore->GetName());
#endif
    for (unsigned num = 0; num < 10; num++)
    {
        printf("*** Thread `%s` is running: iteration %u\n", name, num);
        currentThread->Yield();
    }
#ifdef SEMAPHORE_TEST
    semaphore->V();
    DEBUG('t', "Thread %s is calling V on sempahore %s\n", name, semaphore->GetName());
#endif
    printf("!!! Thread `%s` has finished\n", name);
}

/// Set up a ping-pong between several threads.
///
/// Do it by launching one thread which calls `SimpleThread`, and finally
/// calling `SimpleThread` on the current thread.
void ThreadTestSimple()
{
    Semaphore *semaphore = new Semaphore("thread_test_simple", 3);
    void *argumentos[SIMPLE_NUM_THREADS][2];
    Thread *threads[SIMPLE_NUM_THREADS];

    for (size_t i = 0; i < SIMPLE_NUM_THREADS; i++)
    {
        char *name = new char[64];
        snprintf(name, 64, "thread %ld", i + 1);
        argumentos[i][0] = name;
        argumentos[i][1] = semaphore;

        Thread *newThread = new Thread(name, true, 10 - i);
        newThread->Fork(SimpleThread, argumentos[i]);
        threads[i] = newThread;
    };

    for (size_t i = 0; i < SIMPLE_NUM_THREADS; i++)
    {
        threads[i]->Join();
    }
}
