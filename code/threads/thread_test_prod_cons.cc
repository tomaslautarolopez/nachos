/// Copyright (c) 1992-1993 The Regents of the University of California.
///               2007-2009 Universidad de Las Palmas de Gran Canaria.
///               2016-2021 Docentes de la Universidad Nacional de Rosario.
/// All rights reserved.  See `copyright.h` for copyright notice and
/// limitation of liability and disclaimer of warranty provisions.

#include "thread_test_prod_cons.hh"

#include <stdio.h>

#define PROD_NUM_THREADS 10
#define CONS_NUM_THREADS 10
#define BUFFER_SIZE 10

struct buffer_t
{
    int buffer[BUFFER_SIZE];
    int occupied;
    int nextin;
    int nextout;
    Condition *cond_prod;
    Condition *cond_cons;
    Lock *lock;
};

void Consumer(void *args)
{
    // Reinterpret arg `name` as a string.
    char *name = (char *)(((void **)args)[0]);
    buffer_t *buffer = (buffer_t *)(((void **)args)[1]);

    while (true)
    {
        printf("%s started\n", name);

        buffer->lock->Acquire();

        while (buffer->occupied <= 0)
        {
            printf("%s waitting\n", name);
            buffer->cond_cons->Wait();
            printf("%s waking up\n", name);
        }

        printf("Occupied: %d\nThread %s is popping element %d\n", buffer->occupied, name, buffer->buffer[buffer->occupied - 1]);
        buffer->occupied--;
        buffer->cond_prod->Signal();

        buffer->lock->Release();

        // currentThread->Yield();
    }
}

void Producer(void *args)
{
    char *name = (char *)(((void **)args)[0]);
    buffer_t *buffer = (buffer_t *)(((void **)args)[1]);
    int number;

    while (true)
    {
        printf("%s started\n", name);

        buffer->lock->Acquire();

        while (buffer->occupied + 1 >= BUFFER_SIZE)
        {
            printf("%s waitting\n", name);
            buffer->cond_prod->Wait();
            printf("%s waking up\n", name);
        }

        number = rand() % 100;

        printf("Occupied: %d\nThread %s is adding element %d\n", buffer->occupied, name, number);

        (buffer->buffer)[buffer->occupied] = number;
        buffer->occupied++;
        buffer->cond_cons->Signal();

        buffer->lock->Release();

        // currentThread->Yield();
    }
}

void ThreadTestProdCons()
{

    void *argumentosProd[PROD_NUM_THREADS][2];
    void *argumentosCons[CONS_NUM_THREADS][2];
    struct buffer_t buffer;
    Lock *lock = new Lock("Producers lock");
    Condition *cond_prod = new Condition("Producers condition", lock);
    Condition *cond_cons = new Condition("Consumers condition", lock);

    buffer.occupied = 0;
    buffer.lock = lock;
    buffer.cond_cons = cond_cons;
    buffer.cond_prod = cond_prod;

    for (size_t i = 0; i < PROD_NUM_THREADS; i++)
    {
        char *name = new char[64];
        snprintf(name, 64, "thread producer %ld", i + 1);
        argumentosProd[i][0] = name;
        argumentosProd[i][1] = &buffer;

        Thread *newThread = new Thread(name);
        newThread->Fork(Producer, argumentosProd[i]);
    };

    for (size_t i = 0; i < CONS_NUM_THREADS; i++)
    {
        char *name = new char[64];
        snprintf(name, 64, "thread consumer %ld", i + 1);
        argumentosCons[i][0] = name;
        argumentosCons[i][1] = &buffer;

        Thread *newThread = new Thread(name);
        newThread->Fork(Consumer, argumentosCons[i]);
    };

    // Hack for Join
    while (true)
    {
        printf("Starting\n");

        currentThread->Yield();
    }
}
