#ifndef NACHOS_THREADS_CHANNEL__HH
#define NACHOS_THREADS_CHANNEL__HH

#include "condition.hh"
#include "lock.hh"
#include "lib/list.hh"

class Channel {
public:

    /// Constructor: set up the lock as free.
    Channel(const char *debugName);

    ~Channel();

    void Send(int message);
    void Receive(int *message);


private:
    const char *name;
    List<int> *buffer;
    Lock *lock;
    Condition *senders;
    Condition *receivers;
};

#endif