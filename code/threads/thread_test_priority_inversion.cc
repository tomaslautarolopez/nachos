/// Copyright (c) 1992-1993 The Regents of the University of California.
///               2007-2009 Universidad de Las Palmas de Gran Canaria.
///               2016-2021 Docentes de la Universidad Nacional de Rosario.
/// All rights reserved.  See `copyright.h` for copyright notice and
/// limitation of liability and disclaimer of warranty provisions.

#include "thread_test_priority_inversion.hh"
#include "lock.hh"
#include "system.hh"

#include <stdio.h>
#include <string.h>

#define SIMPLE_NUM_THREADS 5

void PriorityInversionThread(void *args)
{
    Lock *lock = (Lock *)args;

    printf("Thread %s is going to acquire the lock \n", currentThread->GetName());

    lock->Acquire();

    for (unsigned num = 0; num < 10; num++)
    {
        printf("*** Thread `%s` is running: iteration %u\n", currentThread->GetName(), num);
        currentThread->Yield();
    }

    printf("Thread %s is going to release the lock \n", currentThread->GetName());
    lock->Release();
}

void ThreadTestPriorityInversion()
{
    Lock *lock = new Lock("Priority inversion lock");

    Thread *newThreadLow = new Thread("Low priority thread", true, 10);
    newThreadLow->Fork(PriorityInversionThread, (void *)lock);

    currentThread->Yield();

    Thread *newThreadHigh1 = new Thread("High priority thread 1", true, 0);
    newThreadHigh1->Fork(PriorityInversionThread, (void *)lock);

    Thread *newThreadHigh2 = new Thread("High priority thread 2", true, 1);
    newThreadHigh2->Fork(PriorityInversionThread, (void *)lock);

    newThreadLow->Join();
    newThreadHigh1->Join();
    newThreadHigh2->Join();
}
