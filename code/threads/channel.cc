#include "channel.hh"

Channel::Channel(const char *debugName)
{
    name = debugName;
    lock = new Lock(debugName);
    senders = new Condition(debugName, lock);
    receivers = new Condition(debugName, lock);
    buffer = new List<int>();
}

Channel::~Channel()
{
    delete lock;
    delete senders;
    delete receivers;
    delete buffer;
}

void Channel::Send(int message)
{
    lock->Acquire();

    buffer->Append(message);
    receivers->Signal();
    senders->Wait();

    lock->Release();
}

void Channel::Receive(int *message)
{
    lock->Acquire();

    while (buffer->IsEmpty())
    {
        receivers->Wait();
    }
    *message = buffer->Pop();
    senders->Signal();

    lock->Release();
}
